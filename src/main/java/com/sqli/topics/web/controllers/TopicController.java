package com.sqli.topics.web.controllers;

import com.sqli.topics.dao.TopicRepository;
import com.sqli.topics.entities.Topic;
import com.sqli.topics.services.ITopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping()
public class TopicController {

    @Autowired
    private TopicRepository topicRepository;
    @GetMapping()
    public String forward(){
        return "redirect:/topics/list";
    }

    @GetMapping("topics/list")
    public String displayTopics(Model model){
        List<Topic> topics= topicRepository.findAll();
        model.addAttribute("topicsList",topics);
        model.addAttribute("topic",new Topic());
        return "topics/topic-list";
    }

    @PostMapping()
    public String saveTopic(Model model, Topic topic){
        topicRepository.save(topic) ;
        return"redirect:/topics/list";
    }

    @GetMapping("/delete/{id}")
    public String deleteTopic(Model model,@PathVariable String id){
        topicRepository.deleteById(id);
        return"redirect:/topics/list";
    }
}
