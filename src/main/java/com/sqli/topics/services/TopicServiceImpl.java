package com.sqli.topics.services;

import com.sqli.topics.dao.TopicRepository;
import com.sqli.topics.entities.Topic;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicServiceImpl implements ITopicService {
    @Autowired
    private TopicRepository topicRepository;

    @Override
    public Optional<Topic> findTopicById(String id) {
        return topicRepository.findById(id);
    }

    @Override
    public List<Topic> findAllTopics() {
        return topicRepository.findAll();
    }

    @Override
    public Topic addTopic(Topic topic) {
        return topicRepository.save(topic);
    }

    @Override
    public void deleteTopic(String topicId) {
        topicRepository.deleteById(topicId);
    }

    @Override
    public Topic updateTopic(String id, Topic updatedTopic) {
        Topic newTopic=null;
        if(topicRepository.existsById(id)) newTopic=topicRepository.save(updatedTopic);
        else throw new RuntimeException("topic doesn't exists!");

        return newTopic;
    }
}
