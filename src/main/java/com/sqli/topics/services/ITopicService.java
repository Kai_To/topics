package com.sqli.topics.services;

import com.sqli.topics.entities.Topic;

import java.util.List;
import java.util.Optional;

public interface ITopicService {

    Optional<Topic> findTopicById(String id);
    List<Topic> findAllTopics();
    Topic addTopic(Topic topic);
    void deleteTopic(String topicId);
    Topic updateTopic(String id,Topic updatedTopic);
}
