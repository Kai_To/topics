package com.sqli.topics;

import com.sqli.topics.dao.TopicRepository;
import com.sqli.topics.entities.Topic;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class TopicsApplication {

    public static void main(String[] args) {
        SpringApplication.run(TopicsApplication.class, args);
    }
    @Bean
    CommandLineRunner start(TopicRepository topicRepository){

        return args -> {
            topicRepository.save(new Topic(null,"Topic1","myDescription"));
            topicRepository.save(new Topic(null,"Topic2","myDescription"));
            topicRepository.save(new Topic(null,"Topic3","myDescription"));
            topicRepository.save(new Topic(null,"Topic4","myDescription"));


        };

    }
}
