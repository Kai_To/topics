package com.sqli.topics.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@AllArgsConstructor
@NoArgsConstructor @Data
public class Topic {
    @Id @GeneratedValue(strategy =  GenerationType.IDENTITY)
    private String id;
    private String name;
    private String description;

}
